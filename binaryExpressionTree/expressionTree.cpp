#include "expressionTree.h"
#include <string>

using std::endl;
using std::string;

ExpressionTree::ExpressionTree() : BinaryTree() //Creates an empty expression tree
{
	//calls BinaryTree()
}

ExpressionTree::ExpressionTree(std::string exp) : BinaryTree() //Creates an expression tree from the given expression
{	  
	buildTree(exp);
}

void ExpressionTree::setExpression(std::string exp) //Clears the previous tree and builds a new tree from the given expression
{
	destroyTree();//destroy old tree
	buildTree(exp);
}

double ExpressionTree::getResult() const //Returns the results of evaluating the expression tree
{
	return evaluate();
}

void ExpressionTree::printParseTreeInOrder(std::ostream& os) const //Prints the tree using inOrderTraversal. //infix
{
	inOrderTraversal(os);
}

void ExpressionTree::printParseTreePostOrder(std::ostream& os) const //Prints the tree using postOrderTraversal //postfix
{
	postOrderTraversal(os);
}

double ExpressionTree::evaluate() const
{
	return evaluate(root);
}

double ExpressionTree::evaluate(BinaryTreeNode<string>* node) const
{
	double num;

	if (node == NULL)//If the node is null the results should be 0.0
		return 0.0;

	if (isdigit(node->info[0])) //If the node is a number the result should be that number
	{
		string str = node->info;
		num = stod(str);
		return num;
	}
		
	double lhs = evaluate(node->llink);//recursively evaluate the left tree
	double rhs = evaluate(node->rlink);//recursively evaluate the right tree

	if (node->info == "+")//if an operator, result = lhs +-*/^ rhs
		return lhs + rhs;

	if (node->info == "-")
		return lhs - rhs;

	if (node->info == "*")
		return lhs * rhs;

	if (node->info == "/")
		return lhs / rhs;

	if (node->info == "^")
		return pow(lhs, rhs);

	else
		return 0.0;
}


