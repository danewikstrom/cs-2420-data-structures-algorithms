#ifndef H_Expression_Tree
#define H_Expression_Tree

#include "binaryTree.h"

#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>

class ExpressionTree : private BinaryTree<std::string> //public members of BinaryTree become private members of the ExpressionTree class
{
public:
	ExpressionTree();
	ExpressionTree(std::string exp);
	void setExpression(std::string exp);
	double getResult() const;
	void printParseTreeInOrder(std::ostream& os) const;
	void printParseTreePostOrder(std::ostream& os) const;
	double evaluate() const;
	double evaluate(BinaryTreeNode<std::string>* node) const;
};
#endif