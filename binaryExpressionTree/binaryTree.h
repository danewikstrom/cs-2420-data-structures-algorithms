#ifndef H_BinaryTree
#define H_BinaryTree

#include <cstddef>
#include <iostream>
#include <ostream>

using std::string;

template <class T>
struct BinaryTreeNode
{
	T info;
	BinaryTreeNode<T> *llink;
	BinaryTreeNode<T> *rlink;

	BinaryTreeNode() 
	{
		llink = NULL; 
		rlink = NULL;
	}
	BinaryTreeNode(T value) 
	{ 
		info = value; 
		llink = NULL; 
		rlink = NULL; 
	}
};

template <class T>
class BinaryTree
{
public:
	BinaryTree();
	BinaryTree(const BinaryTree<T> &otherTree);
	const BinaryTree<T>& operator= (const BinaryTree<T> &otherTree);
	~BinaryTree();
	void destroyTree();
	void inOrderTraversal(std::ostream& os) const;
	void postOrderTraversal(std::ostream& os) const;
	void buildTree(BinaryTreeNode<T>* node, T value);
	void buildTree(T value);
	bool isOperator(char op);
	int nodeCount() const;
	
protected:
	BinaryTreeNode<T> *root;
	
private:
	int pos;
	void copyTree(BinaryTreeNode<T>* &copiedTreeRoot, BinaryTreeNode<T>* otherTreeRoot);
	void destroy(BinaryTreeNode<T>* &p);
	void inOrder(BinaryTreeNode<T> *p, std::ostream& os) const;
	void postOrder(BinaryTreeNode<T> *p, std::ostream& os, int& count) const;
	int nodeCount(BinaryTreeNode<T>* p) const;
};

template <class T>
BinaryTree<T>::BinaryTree()
{
	root = NULL;
	pos = 0;
}

template <class T>
BinaryTree<T>::BinaryTree(const BinaryTree<T> &otherTree)//copy constructor. Calls copyTree
{
	pos = otherTree.pos;
	if (otherTree.root == NULL)
	{
		root = NULL;
		pos = 0;
	}
		
	else
		copyTree(root, otherTree.root);
}

template<class T>
void BinaryTree<T>::copyTree(BinaryTreeNode<T>* &copiedTreeRoot, BinaryTreeNode<T>* otherTreeRoot)//copy constructor method for deep copy
{
	/*makes a copy of the binary tree to which otherTreeRoot points. The pointer copiedTreeRoot
	points to the root of the copied binary tree.*/
	if (otherTreeRoot == NULL)
		copiedTreeRoot = NULL;
		
	else
	{
		copiedTreeRoot = new BinaryTreeNode<T>;
		copiedTreeRoot->info = otherTreeRoot->info;
		copyTree(copiedTreeRoot->llink, otherTreeRoot->llink);
		copyTree(copiedTreeRoot->rlink, otherTreeRoot->rlink);
		pos = 0;
	}
}

template <class T>
BinaryTree<T>::~BinaryTree()//destructor. Calls destroyTree
{
	destroy(root);
}

template <class T>
void BinaryTree<T>::destroy(BinaryTreeNode<T>* &p)
{
	//function to destroy the binary tree to which p points.
	if (p != NULL)
	{
		destroy(p->llink);
		destroy(p->rlink);
		delete p;
		p = NULL;
		pos = 0;
	}
}

template <class T>
void BinaryTree<T>::destroyTree()
{
	destroy(root);
}

template <class T>
const BinaryTree<T>& BinaryTree<T>:: operator=(const BinaryTree<T> &otherTree)
{
	if (this != &otherTree)//avoid self copy
	{
		if (root != NULL)//if the binary tree is not empty, destroy the binary tree
			destroyTree(root);

		if (otherTree.root == NULL)//otherTree is empty
			root = NULL;

		else
			copyTree(root, otherTree.root);
	}
	return *this;
}

template<class T>
void BinaryTree<T>::inOrder(BinaryTreeNode<T> *p, std::ostream& os) const
{
	/*function to do an inorder traversal of the tree to which p points*/
	if (p != NULL)
	{
		inOrder(p->llink, os);//Traverse the left subtree
		os << p->info;		  //Visit the node
		inOrder(p->rlink, os);//Treaverse the right subtree
	}
}

template<class T>
void BinaryTree<T>::postOrder(BinaryTreeNode<T> *p, std::ostream& os, int& count) const
{
	/*function to do a postorder traversal of the binary tree to which p points*/
	int num = nodeCount();

	if (p != NULL) 
	{
		postOrder(p->llink, os, count);//Traverse the left subtree
		postOrder(p->rlink, os, count);//Traverse the right subtree
		if (count >= num && p != NULL)
		{
			os << p->info;				//to get rid of last whitespace char
			return;
		}
		count++;
		os << p->info << " ";			//visit
	}
}

template <class T>
void BinaryTree<T>::inOrderTraversal(std::ostream& os) const
{
	inOrder(root, os);
}

template <class T>
void BinaryTree<T>::postOrderTraversal(std::ostream& os) const
{
	int count = 1;
	postOrder(root, os, count);	//pass by reference to use counter in the recursive postorder function
}

template <class T>
void BinaryTree<T>::buildTree(BinaryTreeNode<T>* node, T value)
{
	string temp;

	while (value[pos])
	{
		if (value[pos] == '(')
		{
			node->llink = new BinaryTreeNode<T>();//put a new node to the left
			pos++;

			if (node->llink)
			{
				buildTree(node->llink, value);//recursivley go left
			}
		}
		else if (isdigit(value[pos]))
		{
			temp = "";
			while (isdigit(value[pos]))//to get numbers that are more than one digit
			{
				temp += value[pos];
				pos++;
			}
			node->info = temp;//set the current node's info to that digit
			return;
		}

		else if (isOperator(value[pos]))
		{
			string val(1, value[pos]);//to make the char a string
			node->info = val;
			node->rlink = new BinaryTreeNode<T>();//put a new node to the right
			pos++;
			if (node->rlink)
			{
				buildTree(node->rlink, value);//recursivley go right
			}
		}

		else if (value[pos] == ')')
		{
			pos++;
			return;
		}
	}
}

template <class T>
void BinaryTree<T>::buildTree(T value)
{
	if (root)
		buildTree(root, value);
	else
	{
		root = new BinaryTreeNode<T>(value);//if root is null then create a new node
		buildTree(value);
	}
}

template <class T>
bool BinaryTree<T>::isOperator(char op) //checks if char is an operator
{
	if (op == '+' || op == '-' || op == '*' || op == '/' || op == '^')
		return true;
	else
		return false;
}

template <class T>
int BinaryTree<T>::nodeCount() const
{
	return nodeCount(root);
}

template <class T>
int BinaryTree<T>::nodeCount(BinaryTreeNode<T>* p) const
{
	if (p != NULL)
	{
		return nodeCount(p->llink) + nodeCount(p->rlink) + 1; 
	}
	else
		return 0;
}
#endif
