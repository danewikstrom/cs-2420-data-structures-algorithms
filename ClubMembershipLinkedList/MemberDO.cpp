#include "MemberDO.h"
#include <iostream>
#include <fstream>

using namespace std;

MemberDO::MemberDO()
{
	key = 0;
	lastName = "";
	firstInit = 0;
	dues = 0.0;
}

MemberDO::MemberDO(int k, string ln, char fi, double d)
{
	//initializes each data member to a specific value passed as a parameter.
	key = k;
	lastName = ln;
	firstInit = fi;
	dues = d;
}

int MemberDO::getKey() const
{
	return key;
}

void MemberDO::setKey(int k)
{
	key = k;
}

string MemberDO::getLastName() const
{
	return lastName;
}

void MemberDO::setLastName(string ln)
{
	lastName = ln;
}

char MemberDO::getFirstInitial() const
{
	return firstInit;
}

void MemberDO::setFirstInitial(char fi)
{
	firstInit = fi;
}

double MemberDO::getDues() const
{
	return dues;
}

void MemberDO::setDues(double d)
{
	dues = d;
}

void MemberDO::print() const
{
	
}

void MemberDO::readFromFile(const char* file, OrderedLinkedList<MemberDO>& ll)
{
	/*The first argument is the filename that contains data for existing members. This method should read the data for each individual member 
	from the input file(one line of data per member), create a new MemberDO object, and insert this object into the linked list specified in 
	the second argument. Implelmenting this method is extra - credit.You can use the implementation provided which just inserts the data without 
	reading it from a file if you do not want the extra credit.*/

	MemberDO data;
	data.key = 6789;
	data.lastName = "Towson";
	data.firstInit = 'J';
	data.dues = 65.25;
	ll.insert(data);
	data.key = 3456;
	data.lastName = "Johns";
	data.firstInit = 'K';
	data.dues = 200.00;
	ll.insert(data);
	data.key = 1123;
	data.lastName = "Stevens";
	data.firstInit = 'M';
	data.dues = 112.35;
	ll.insert(data);
	data.key = 4489;
	data.lastName = "Ellwood";
	data.firstInit = 'B';
	data.dues = 700.25;
	ll.insert(data);
	data.key = 5555;
	data.lastName = "Pryor";
	data.firstInit = 'R';
	data.dues = 99.99;
	ll.insert(data);
	//std::ifstream memberData(file);	/**** EXTRA CREDIT ATTEMPT, ENCOUNTERS RUNTIME ERROR *****/

	//std::string lastname;
	//int key;
	//char fInit;
	//double dues;

	//while (memberData >> lastname >> key >> fInit >> dues)
	//{
	//	ll.insert(MemberDO(key, lastname, fInit, dues));
	//}
}

