#ifndef ORDEREDLINKEDLIST_H
#define ORDEREDLINKEDLIST_H

template <class Type>
struct Node
{
	Type info;
	Node *next;
};


template <class Type>
class OrderedLinkedList
{
public:
	OrderedLinkedList();
	OrderedLinkedList(const OrderedLinkedList& other);
	~OrderedLinkedList();
	OrderedLinkedList<Type>& operator=(const OrderedLinkedList<Type>& other);
	int insert(const Type&);
	Type* find(int) const;
	Type* get(int) const;
	int remove(int);
	void clear();
	int size() const;
	void print() const;
	
private:
	Node<Type> *first;//a pointer to the first node in the list
	Node<Type> *last;//a pointer to the last node in the list
	int count;//a count of the number of nodes in the list
	void copyList(const OrderedLinkedList& other);
	void deleteList(Node<Type>* start);
};

template <class Type>
OrderedLinkedList<Type>::OrderedLinkedList()
{
	//initialize all data members
	first = NULL;
	last = NULL;
	count = 0;
}

template <class Type>
OrderedLinkedList<Type>::OrderedLinkedList(const OrderedLinkedList<Type>& other)
{
	first = NULL;
	copyList(other);
}

template <class Type>
OrderedLinkedList<Type>::~OrderedLinkedList()
{
	deleteList(first);
}


template <class Type>
OrderedLinkedList<Type>& OrderedLinkedList<Type>::operator=(const OrderedLinkedList& other)
{
	if (this != &other)//avoid self copy
	{
		copyList(other);
	}

	return *this;
}

template <class Type>
void OrderedLinkedList<Type>::copyList(const OrderedLinkedList& other)
{
	Node<Type>* newNode;
	Node<Type>* current;

	if (first != NULL)//if the list is nonempty, make it empty
	{
		deleteList(first);
	}

	if (other.first == NULL)//otherlist is empty
	{
		first = NULL;
		last = NULL;
		count = 0;
	}

	else
	{
		current = other.first;
		count = other.count;
		first = new Node<Type>;//create the node
		first->info = current->info;//copy the info
		first->next = NULL;//set the next field of the node to NULL
		last = first;//make last point to the first node
		current = current->next;//make current point to the next node

		while (current != NULL)//copy the remaining list
		{
			newNode = new Node<Type>;//create a new node
			newNode->info = current->info;//copy the info
			newNode->next = NULL;//set next of newNode to NULL
			last->next = newNode;//attach newNOde after last
			last = newNode;//make last point to the actual last node
			current = current->next;//make current point to the next node
		}
	}
}

template <class Type>
void OrderedLinkedList<Type>::deleteList(Node<Type>* start)
{
	Node<Type>* temp = start;
	Node<Type>* deleteMe = NULL;
	
	while (temp != NULL)
	{
		deleteMe = temp;
		temp = temp->next;
		delete deleteMe;
	}

	last = NULL;
	count = 0;
}

template <class Type>
int OrderedLinkedList<Type>::insert(const Type& newItem)/*MEMORY ERROR*/
{
	Node<Type>* current;//pointer to traverse the list
	Node<Type>* trailCurrent = NULL;//pointer just before current
	Node<Type>* newNode;//pointer to create a node

	bool found;

	newNode = new Node<Type>;//create the node
	newNode->info = newItem;//store newItem in the node
	newNode->next = NULL;//set the next field of the node to NULL
	//the list is initiall empty. the node containing the new item is the only node and, thus, the first node in the list.
	if (first == NULL)
	{
		first = newNode;
		last = newNode;
		count++;
	}

	else
	{
		current = first;
		found = false;

		while (current != NULL && !found)//search the list
		{
			if (current->info.getKey() == newItem.getKey())//checks for duplicates
			{
				return -1;
			}
			if (current->info.getKey() >= newItem.getKey()) //compares key values
			{
				found = true;
			}
			else//move to next node
			{
				trailCurrent = current; 
				current = current->next;
			}
		}
		//the new item is smaller than the smallest item in the list. The new item goes at the beginning of the list. We need to adjust the list's
		//first pointer, also count is ++.
		if (current == first)
		{
			newNode->next = first;
			first = newNode;
			count++;
		}
		//the new item is to be inserted somewhere in the list
		else
		{
			trailCurrent->next = newNode;
			newNode->next = current;

			if (current == NULL)
			{
				last = newNode;
			}

			count++;
		}
	}
	int keyValue = newNode->info.getKey();	
	return keyValue;
	delete newNode;
}

template <class Type>
Type* OrderedLinkedList<Type>::get(int dest) const
{
	if (dest > count - 1 || dest < 0)
	{
		return NULL;
	}

	Node<Type>* current;
	int currCount = 0;
	current = first;

	while (currCount < dest) 
	{
		current = current->next;
		currCount++;
	}

	return &(current->info);
}

template <class Type>
Type* OrderedLinkedList<Type>::find(int dest) const
{
	if (dest < 0) 
	{ 
		return NULL; 
	}
	
	bool found = false;
	Node<Type>* current;//pointer to traverse the list
	current = first;
	
	while (current != NULL && current->info.getKey() != dest)
	{
		if (current == NULL)
		{
			return NULL;
		}
		current = current->next;
	}

	return &(current->info);
}

template <class Type>
int OrderedLinkedList<Type>::remove(int key)
{
	Node<Type> *current = NULL; //pointer to traverse the list
	Node<Type> *trailCurrent = NULL; //pointer just before current
	bool found;

	if (first == NULL) //List is initially empy, We cannot delete from an empy list
	{
		return -1;
	}
	else
	{
		current = first;
		found = false;

		while (current != NULL && !found)  //search the list
		{
			if (current->info.getKey() >= key)
			{
				found = true;
			}
			else
			{
				trailCurrent = current;
				current = current->next;
			}
		}

		if (current == NULL)   //The list is not empty but the item to be deleted is not in the list
		{
			return -1;
		}
		else
		{
			if (current->info.getKey() == key) //the item to be deleted is in the list										 
			{
				if (first == current)//Item to be delteed is contained in the first node. Must adjust the first pointer of the list
				{
					first = first->next;

					if (first == NULL)
					{
						last = NULL;
					}

					delete current;
				}
				else//Item to be deleted is somewhere in the list. Current point to node containing the item to be deleted, & trailCurrent
					//point to the node just before the node pointed to by current
				{
					trailCurrent->next = current->next;

					if (current == last)
					{
						last = trailCurrent;
					}

					delete current;
				}
				count--;
				return key;
			}
			else//item to be deleted is not in the list
			{
				return -1;
			}
		}
	}
}

template <class Type>
void OrderedLinkedList<Type>::clear()
{
	first = NULL;
	last = NULL;
	count = 0;
}


template <class Type>
int OrderedLinkedList<Type>::size() const
{
	if (first != NULL)
	{
		return count;
	}	
	else
	{
		return 0;
	}
	return -1;
}

template <class Type>
void OrderedLinkedList<Type>::print() const
{
	Node<Type> *current;
	
	current = first;
	while (current != NULL)
	{
		current = current->next;
	}
}
#endif
