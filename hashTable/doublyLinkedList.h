#ifndef DOUBLYLINKEDLIST_H
#define DOUBLYLINKEDLIST_H

//--------------------------------------------
//The Node class
//--------------------------------------------
template <typename T, typename U>
class Node
{
public:
	T key;
	U info;//value
	Node *backward;
	Node *forward;
};


//--------------------------------------------
//For error handling
//--------------------------------------------
class Error {};


//--------------------------------------------
//The linked list base class.
//--------------------------------------------
//Complete the methods indicated by TODO below.  Implement a doubly linked list.  These methods must work as specified in the assignment.
template <typename T, typename U>
class DoublyLinkedList
{
private:
	int count;
	Node<T, U> *first;
	Node<T, U> *last;

	void destroyList();

public:

	DoublyLinkedList(const DoublyLinkedList& other);
	DoublyLinkedList();
	~DoublyLinkedList();
	int getCount() const;
	void insertLast(const T&, const U&);
	bool nodeWithKeyExists(const T&) const;
	U& searchForKey(const T&);
	void deleteNodeWithKey(const T&);
};

template <typename T, typename U>
DoublyLinkedList<T, U>::DoublyLinkedList()
{
	first = NULL;
	last = NULL;
	count = 0;
}

//copy constructor
template <typename T, typename U>
DoublyLinkedList<T, U>::DoublyLinkedList(const DoublyLinkedList<T, U>& other)
{
	if (other.first == NULL)
	{
		this->first = NULL;
		this->last = NULL;
		count = 0;
		return;
	}

	if (other.first != NULL)
	{
		Node<T, U> *temp = new Node<T, U>();
		temp->key = other.first->key;
		temp->info = other.first->info;
		temp->backward = NULL;
		temp->forward = NULL;
		this->first = temp;
	}

	//start at the second node now
	Node<T, U> *previous = other.first;
	Node<T, U> *current = other.first->forward;
	while (current != NULL)
	{
		Node<T, U> *temp = new Node<T, U>();
		temp->key = current->key;
		temp->info = current->info;
		temp->backward = previous;
		previous->forward = temp;
		temp->forward = NULL;
		previous = current;
		current = current->forward;
	}
	last = previous;

	this->count = other.count;
}

template <typename T, typename U>
DoublyLinkedList<T, U>::~DoublyLinkedList()
{
	this->destroyList();
}

//Destroy the list, and reclaim all memory.
template <typename T, typename U>
void DoublyLinkedList<T, U>::destroyList()
{
	Node<T, U> *temp;
	while (first != NULL)
	{
		temp = first;
		first = first->forward;
		delete temp;
	}
	last = NULL;
	count = 0;
}

//Returns the number of nodes in this linked list.
template <typename T, typename U>
int DoublyLinkedList<T, U>::getCount() const
{
	return count;
}
//Done
//Add/insert a node onto the end of the list.
template <typename T, typename U>
void DoublyLinkedList<T, U>::insertLast(const T& key, const U& value)
{
	//TODO: Complete this: method should add a node to the end of the list.
	Node<T, U>* newNode = new Node<T, U>;//create a new node	

	newNode->info = value;
	newNode->key = key;
	newNode->forward = NULL;
	newNode->backward = NULL;

	if (first == NULL)//if the list is empty, newNode is both the first and last node
	{
		first = newNode;
		last = newNode;
		count++;
	}
	else//the list is not empty, insert newNode after last
	{
		last->forward = newNode;//insert newNode after last
		last = newNode;//make last point to the actual last node in the list
		count++;
	}
}

//Done
template <typename T, typename U>
bool DoublyLinkedList<T, U>::nodeWithKeyExists(const T& key) const
{
	//TODO: Complete this: method determines if the list has a node with a matching key. If so, return true; otherwise, return false.
	Node<T, U>* current = first;
	bool found = false;

	while (current != NULL && !found)
		if (current->key == key)
			found = true;
		else
			current = current->forward;

	return found;
}


//Done
template <typename T, typename U>
U& DoublyLinkedList<T, U>::searchForKey(const T& key)
{
	//TODO: Complete this: method determines if the list has a node with a matching key. If so, return a reference to the info; otherwise, throw an Error object.
	Node<T, U>* current = first; //pointer to traverse the list
	bool found = false;

	while (current != NULL && !found)
	{
		if (current->key == key)
			found = true;

		else
			current = current->forward;
	}

	if (found)
	{
		return current->info;
	}

	//throw an error if there's nothing to delete
	else
		throw Error();
}


//Done
template <typename T, typename U>
void DoublyLinkedList<T, U>::deleteNodeWithKey(const T& key)
{
	//TODO: Complete this: determines if the list has a node with a matching key. If so, this node is deleted.
	Node<T, U>* current;//pointer to traverse the list
	Node<T, U>* trailCurrent;//pointer just before current

	bool found;

	if (first->key == key)
	{
		current = first;
		first = first->forward;

		if (first != NULL)
			first->backward = NULL;
		else
			last = NULL;

		count--;

		delete current;
	}

	else
	{
		found = false;
		current = first;

		while (current != NULL && !found)//search the list
		{
			if (current->key == key)
				found = true;
			else
				current = current->forward;

			if (current->key == key)//check for equality
			{
				trailCurrent = current->backward;
				trailCurrent->forward = current->forward;

				if (current->forward != NULL)
					current->forward->backward = trailCurrent;

				if (current == last)
					last = trailCurrent;

				count--;
				delete current;
			}
		}
	}
}
#endif
