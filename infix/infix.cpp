#include "infix.h"
#include <stack>

using std::string;

Infix::Infix()
{
	pfx = "";
	infx = "";
}

Infix::Infix(string infix)
{
	infx = infix;
	pfx = convertToPostfix(infx);
}

void Infix::setInfix(string infix)
{
	infx = infix;
	pfx = convertToPostfix(infx);
}

string Infix::getInfix() const
{
	return infx;
}

string Infix::getPostfix() const
{
	return pfx;
}

int Infix::getNumberOfOperators() const
{
	int numOfOperators = 0;

	for (int i = 0; i < infx.length(); i++)
	{
		if (infx[i] == '+')
			numOfOperators++;
		else if (infx[i] == '-')
			numOfOperators++;
		else if (infx[i] == '*')
			numOfOperators++;
		else if (infx[i] == '/')
			numOfOperators++;
		else if (infx[i] == '^')
			numOfOperators++;
	}
	return numOfOperators;
}

int Infix::getNumberOfOperands() const//numbers
{
	int numOfOperands = 0;

	for (int i = 0; i < infx.length(); i++)
	{
		if (infx[i] == ' ')
			continue;
		else if (infx[i] == '+')
			continue;
		else if (infx[i] == '-')
			continue;
		else if (infx[i] == '*')
			continue;
		else if (infx[i] == '/')
			continue;
		else if (infx[i] == '^')
			continue;
		else if (infx[i] == '(')
			continue;
		else if (infx[i] == ')')
			continue;
		else
			numOfOperands++;
	}

	return numOfOperands;
}

string Infix::convertToPostfix(string infix)
{
	pfx = "";//initialize to an empty string
	std::stack<string> stack;//initialize the stack.
	std::string sym;//can be an operator, a parenthesis (left or right), a single digit, or an entire number. Not whitespaces.

	if (infx.empty())
		return "";

	for (int i = 0; i < infx.length(); i++)
	{
		sym = infx[i];

		if (isdigit(sym[0]))//check if current sym is a digit. Only holds one digit at a time and must use char for isdigit, therefore positional value of 0;
			pfx.append(sym);//append sym to pfx

		if (sym == "(")//If sym is '(', push sym into the stack
			stack.push(sym);

		if (sym == ")")//pop and append all the symbols from the stack until the most recent left parenthesis. Pop and discard the left parenthesis
		{
			do
			{
				pfx.append(stack.top());//append whatever is at the top of the stack to pfx
				stack.pop();// to get to next thing in stack

			} while (stack.top() != "(");//loops till it reaches the most recent left parenthesis

			stack.pop(); // Pop and discard the left parenthesis.
		}

		if (sym == "+" || sym == "-" || sym == "*" || sym == "/" || sym == "^")//if sym is an operator
		{
			pfx.append(" ");

			if (stack.empty())//no need to compare if the stack is empty
			{
				stack.push(sym);
				stack.push(" ");
			}

			else if (precedence(stack.top()[0]) < precedence(sym[0]))//check if current operator in stack is of higher precedence
			{
				stack.push(sym);
				stack.push(" ");
			}

			else
			{
				do
				{
					pfx.append(stack.top());
					stack.pop();
				} while (!stack.empty());

				stack.push(sym);// Push sym onto the stack	
			}
		}//end if
	} //end for

	while (!stack.empty()) // some operators might be left in the stack. Pop and append to pfx everything from the stack.
	{
		pfx.append(stack.top());
		stack.pop();
	}

	return pfx;
}

void Infix::clear()
{
	if (!infx.empty())
		infx.clear();
	if (!pfx.empty())
		pfx.clear();
}

int Infix::precedence(char oper)
{
	switch (oper)//Please Excuse My Dear Aunt Sally
	{
	case '(':
		return 0;
		break;
	case '^':
		return 1;
		break;
	case '*':
		return 2;
		break;
	case '/':
		return 2;
		break;
	case '+':
		return 3;
		break;
	case '-':
		return 3;
		break;
	default:
		return 0;
		break;
	}
}



