#ifndef INFIX_H
#define INFIX_H
#include <string>
#include <stack>

class Infix
{
public:
	Infix();// Default constructor  stores an empty infix / postfix expression
	Infix(std::string); // constructor takes a string parameter as an infix expression, stores the infix expression and sets the postfix expression.
	void setInfix(std::string); // stores the infix expression, and updates the postfix expression.
	std::string getInfix() const; // retrieves the infix expression
	std::string getPostfix() const; // retrieves the postfix expression
	int getNumberOfOperators() const; // returns the number of operators contained in the expression
	int getNumberOfOperands() const; // returns the number of operands contained in the expression
	std::string convertToPostfix(std::string); // Converts the infix expression into a postfix expression.The resulting postfix expression is stored in pfx.
	void clear(); // resets the infix/postfix expression to an empty string	
	int precedence(char op); // Determines the order of precedence of an operand

private:
	std::string pfx;
	std::string infx;
};
#endif